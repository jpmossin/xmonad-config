import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ICCCMFocus
import XMonad.StackSet as W
import System.IO
import XMonad.Actions.Volume (raiseVolumeChannels, lowerVolumeChannels, toggleMuteChannels)
import XMonad.Actions.WorkspaceNames
import XMonad.Prompt

myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

main = do
    
    xmproc <- spawnPipe "xmobar /home/jp/.xmonad/xmobarrc"
    xmonad $ defaultConfig
        { manageHook = manageDocks <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $  layoutHook defaultConfig
        , logHook = takeTopFocus >> dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , modMask = mod4Mask     -- Rebind Mod to the Windows key
	, borderWidth=2
	, terminal= "gnome-terminal"
	, startupHook = setWMName "LG3D" >> spawn "~/.xmonad/divsetup"
       	, normalBorderColor  = "#000000"
       	, focusedBorderColor = "red"
	--, workspaces = myWorkspaces
	}
	`additionalKeys`
             ([((mod4Mask, xK_m ), spawn "dmenu_run"),
	       ((mod4Mask, xK_F2 ), lowerVolumeChannels ["Master"] 3 >> spawn "ogg123 ~/.xmonad/volumechange.ogg"),
	       ((mod4Mask, xK_F3 ), raiseVolumeChannels ["Master"] 3 >> spawn "ogg123 ~/.xmonad/volumechange.ogg"),
	       ((mod4Mask, xK_F1 ), toggleMuteChannels ["Master","Headphone", "PCM"] >> return ()),
	       ((mod4Mask, xK_w ), renameWorkspace defaultXPConfig)
	     ] 
	      ++
              
          --  
          -- mod-[1..9], Switch to workspace N
          -- mod-shift-[1..9], Move client to workspace N
          -- 
	      [((m .|. mod4Mask, k), windows $ f i)
              | (i, k) <- zip (myWorkspaces) [xK_1 .. xK_9]
              , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]])

	   